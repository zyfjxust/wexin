package com.zhang.weixin.generator;




/**
 * 代码生成类:
 * Created by BG235729 on 2017/1/10.
 * 使用方法：建立包com.best.proxy.{PlATFORMCODE}
 * 运行main函数，刷新包即可
 */
public class Generator {

	// 根据命名规范，只修改此常量值即可
	private static String PlATFORMCODE = "mgj";
	
	private static String MODULE = "D:/bg235729/workspace/weixin/weixin-common";
	
	

	/**
	 * 自动代码生成
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		MybatisGeneratorUtil.generator(PlATFORMCODE, MODULE);
	}

}
