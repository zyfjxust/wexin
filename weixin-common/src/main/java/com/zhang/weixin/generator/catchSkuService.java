package com.zhang.weixin.generator;

import java.util.ArrayList;
import java.util.List;

import com.best.oasis.erp.edi.proxy.bizdata.SkuIn;
import com.best.oasis.erp.edi.proxy.bizdata.SkuOut;
import com.best.proxy.vo.oauth.OauthVO;
import com.best.proxy.vo.sku.SkuTmpVo;
import com.best.proxy.vo.sku.SkuVO;


public abstract class catchSkuService {

	/**
	 * 根据授权抓商品ID
	 * @param vo
	 * @return
	 */
	public List<String> getSkuIds(OauthVO vo){
		List<String> strList = new ArrayList<>();
		return strList;
	}	
	
	/**
	 * 根据更新时间抓商品ID
	 * @param vo
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public List<String> getSkuIds(OauthVO vo, String startTime, String endTime){
		List<String> strList = new ArrayList<>();
		return strList;
	}
	
	
	/**
	 * 抓商品列表，含ID列表和商品详情
	 * @param vo
	 * @return
	 */
	public List<SkuTmpVo> getSkus(OauthVO vo){
		List<SkuTmpVo> strList = new ArrayList<>();
		return strList;
	};
	
	
	/**
	 * 根据更新时间抓ID列表和商品详情
	 * @param vo
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public List<SkuTmpVo> getSkus(OauthVO vo, String startTime, String endTime){
		List<SkuTmpVo> strList = new ArrayList<>();
		return strList;
	};
	
	/**
	 * 根据商品ID/skucode抓单个商品详情
	 * @param vo
	 * @param numid
	 * @return
	 */
	public SkuTmpVo getSkusById(OauthVO vo, String numid){
		SkuTmpVo strList = new SkuTmpVo();
		return strList;
	};
	
	/**
	 * 下发Request商品，下发failure商品，手工同步商品：将抓到的订单详情组合成ERP的原始商品
	 * @param skuVO
	 * @param vo
	 * @return
	 */
	public List<SkuIn> skuData2ErpSku(SkuVO skuVO, OauthVO vo){
		List<SkuIn> strList= new ArrayList<>();
		return strList;
	};
		
	/**
	 * 同步商品库存
	 * @param vo
	 * @param skuOut
	 */
	public void updateSkuInv(OauthVO vo, SkuOut skuOut){
		
	};
}
