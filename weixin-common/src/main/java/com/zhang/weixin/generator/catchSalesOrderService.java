package com.zhang.weixin.generator;

import java.util.List;

import com.best.oasis.erp.edi.proxy.bizdata.SoIn;
import com.best.oasis.erp.edi.proxy.bizdata.SoOut;
import com.best.proxy.vo.oauth.OauthVO;
import com.best.proxy.vo.salesOrder.SalesOrderVO;

public interface catchSalesOrderService {
	
	/**
	 * 根据订单更新时间抓订单列表IDs
	 * @param vo
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public List<String> getSalesOrderIds(OauthVO vo, String startTime, String endTime);
		
	/**
	 * 根据订单更新时间抓取订单列表ids和订单详情
	 * @param vo
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public List<SalesOrderVO> getSalesOrders(OauthVO vo, String startTime, String endTime);

	/**
	 * 更加订单id抓订单详情
	 * @param vo
	 * @param numId
	 * @return
	 */
	public SalesOrderVO getSalesOrderInfoById(OauthVO vo, String numId);
	
	/**
	 * 发货反馈
	 * @param feedBack
	 * @param vo
	 */
	public void feedBack(SoOut feedBack, OauthVO vo);
	
	/**
	 * 获取时间段内的订单数量
	 * @param vo
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public Long getSalesOrderCountIdsByHand(OauthVO vo, String startTime, String endTime);
	
	/**
	 * 下发失败订单一次后还是失败，检查订单状态，是否关闭下发，一般是已经完成的订单就不再下发
	 * @param salesOrder
	 * @return
	 */
	public boolean needToStop(SalesOrderVO salesOrder);
	
	/**
	 * 下发Request订单，failure订单，手工同步订单时，将原始的订单详情组合成ERP原始订单
	 * @param salesOrderVO
	 * @param vo
	 * @return
	 */
	public SoIn beibeiSoData2ErpSo(SalesOrderVO salesOrderVO, OauthVO vo);
	
	/**
	 * 订单从proxy到ERP失败后对返回的错误信息解析
	 * @param error
	 * @param salesOrder
	 * @return
	 */
	public String erpErrorPrase(String error, SalesOrderVO salesOrder);
}
