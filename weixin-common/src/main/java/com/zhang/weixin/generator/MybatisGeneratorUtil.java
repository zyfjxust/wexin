package com.zhang.weixin.generator;


import org.apache.velocity.VelocityContext;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;



/**
 * 代码生成类
 * Created by ZhangShuzheng on 2017/1/10.
 */
public class MybatisGeneratorUtil {

	// generatorConfig模板路径
	private static String generatorConfig_vm = "/template/generatorConfig.vm";
	// Service模板路径
	private static String service_vm = "/template/Service.vm";
	// ServiceMock模板路径
	private static String serviceMock_vm = "/template/ServiceMock.vm";
	// ServiceImpl模板路径
	private static String skuServiceImpl_vm = "/template/SkuServiceImpl.vm";
	
	private static String OrderServiceImpl_vm = "/template/OrderServiceImpl.vm";

	/**
	 * 
	 * @param platformCode  平台 
	 */
	public static void generator(String platformCode, String module) throws Exception{		
		skuServiceImpl_vm = MybatisGeneratorUtil.class.getResource(skuServiceImpl_vm).getPath().replaceFirst("/", "");
		OrderServiceImpl_vm = MybatisGeneratorUtil.class.getResource(OrderServiceImpl_vm).getPath().replaceFirst("/", "");
		
		String firstUpCasePlatform = platformCode.substring(0, 1).toUpperCase()+platformCode.substring(1);
		String upCasePlatform = platformCode.toUpperCase();
		
		
		
		System.out.println("========== 开始生成Service ==========");
		String ctime = new SimpleDateFormat("yyyy/M/d").format(new Date());
		String serviceImplPath = module+"/src/main/java/"+"com/zhang/weixin/generator" + platformCode;
		String skuServiceImpl = serviceImplPath + "/" + firstUpCasePlatform + "SkuServiceImpl.java";
		String orderServiceImpl = serviceImplPath + "/" + firstUpCasePlatform + "OrderServiceImpl.java";
		// 生成serviceImpl
		//File skuServiceImplFile = new File(skuServiceImpl);
		//File orderServiceImplFile = new File(orderServiceImpl);
//		if (!skuServiceImplFile.exists()) {
//			VelocityContext context = new VelocityContext();
//			context.put("platformCode", platformCode);
//			context.put("Platform", platformCode.substring(0, 1).toUpperCase()+platformCode.substring(1));
//			context.put("ctime", ctime);
//			VelocityUtil.generate(serviceImpl_vm, serviceImpl, context);
//			System.out.println(serviceImpl);
//		}
		
		VelocityContext context = new VelocityContext();
		context.put("platformCode", platformCode);
		context.put("firstUpCasePlatform", firstUpCasePlatform);
		context.put("upCasePlatform", upCasePlatform);
		context.put("ctime", ctime);
		VelocityUtil.generate(skuServiceImpl_vm, skuServiceImpl, context);
		System.out.println(skuServiceImpl);
		VelocityUtil.generate(OrderServiceImpl_vm, orderServiceImpl, context);
		System.out.println(orderServiceImpl);
		System.out.println("========== 结束生成Service ==========");
	}

	// 递归删除非空文件夹
	public static void deleteDir(File dir) {
		if (dir.isDirectory()) {
			File[] files = dir.listFiles();
			for (int i = 0; i < files.length; i++) {
				deleteDir(files[i]);
			}
		}
		dir.delete();
	}

}
